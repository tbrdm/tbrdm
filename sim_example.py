"""sim_example.py
Example for simulation of different systems

"""

import TBRDM.sim as sim
import matplotlib.pyplot as plt
import numpy as np
from os import mkdir

#Simulation flags
FLAG_NO_SAVE = 0
FLAG_NO_OUTPUT = 1
FLAG_NO_ALERT = True
FLAG_SHOW_MEMORY = True
FLAG_FULL_RDM =  False
FLAG_NO_OBRDM = False
FLAG_NO_GRDM = False
FLAG_NO_RRDM = False

sim1 = sim.Simulation()

#Simulation parameters
N = 10
Nu = 2
Nd = 3
tx = 1
ty = 1
U = 1
Vx = 0.5
Vy = 0.5
one_dim=True
sim1.set_flags(FLAG_NO_SAVE,FLAG_NO_OUTPUT,FLAG_NO_ALERT,FLAG_SHOW_MEMORY,FLAG_FULL_RDM,FLAG_NO_OBRDM,FLAG_NO_GRDM, FLAG_NO_RRDM)



DIR_TOP = "results/hubbard_chain_test/"
DIR_ROOT="hubbard_chain_U"+str(U)+"_Nu"+str(Nu)+"_Nd"+str(Nd)+"/"
try:
    mkdir(DIR_TOP)
except FileExistsError:
    pass
try:
    mkdir(DIR_TOP+"/"+DIR_ROOT)

except FileExistsError:
    pass

DIR_NAME = DIR_TOP+ "/"+DIR_ROOT+"N"+str(N)+"U"+str(U)+"Nu"+str(Nu)
sim1.simulate(N,Nu,Nd,tx,ty,Vx,Vy,U,one_dim,DIR_NAME)

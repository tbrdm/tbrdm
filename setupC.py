from distutils.core import setup
from Cython.Build import cythonize
import Cython.Compiler.Options
Cython.Compiler.Options.annotate= True
import numpy

setup(name='Lanczos',
ext_modules=cythonize("TBRDM/Lanczos.pyx"),
include_dirs=[numpy.get_include()],
extra_compile_args=["-ffast-math"])

setup(name='Hamiltonian',
ext_modules=cythonize("TBRDM/Hamiltonian.pyx"),
include_dirs=[numpy.get_include()],
extra_compile_args=["-ffast-math"])

setup(name='reduced_RDM',
ext_modules=cythonize("TBRDM/reduced_RDM.pyx"),
include_dirs=[numpy.get_include()],
extra_compile_args=["-ffast-math"])

setup(name='bithacks',
ext_modules=cythonize("TBRDM/bithacks.pyx"),
include_dirs=[numpy.get_include()],
extra_compile_args=["-ffast-math"])

# cython: language_level=3, boundscheck=False,initializedcheck=False, nonecheck=False

""" Bithacks.pyx
Author: Jonas Hachmeister
Last Change: 08.2020
About:
Bitwise operations needed to calculate the 2-RDM

Code for counting set bits by lookup table from: https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetTable
 """

import math
import numpy as np
cimport numpy as np

cdef list BitsSetTable256 = [0] * 256



cpdef void init():
    "Initialize lookup table"
    BitsSetTable256[0] = 0
    cdef int i
    for i in range(256):
        BitsSetTable256[i] = (i & 1) + BitsSetTable256[i // 2]

cpdef tuple list_to_bin(list l):
    "Convert list of binarys to tuple of integers"
    cdef list out = list()
    cdef list byte
    cdef int bit, bin
    cdef int i
    for byte in l:
        i=0
        bin=0
        for bit in reversed(byte):
            bin=bin+bit*2**i
            i=i+1
        out.append(bin)
    return tuple(out)

cpdef int countSetBits(int n):
    "Count set bits"
    return (BitsSetTable256[n & 0xff] +
            BitsSetTable256[(n >> 8) & 0xff] +
            BitsSetTable256[(n >> 16) & 0xff] +
            BitsSetTable256[n >> 24])

cpdef int get_i_bit(int i,int n,int N):
    "Return bit at position i"
    return int((n & (1<<(N-i-1)))>0)

cpdef int flip_i_bit(int i,int n,int N):
    "Flip bit at position i"
    return ((1<<(N-i-1))^n)

cpdef int get_leftmost_bit_pos(int n,int N):
    "Return position of leftmost set bit for a binary number with N digits"
    return N-int(math.log2(n))-1

cpdef int get_rightmost_bit_pos(int n,int N):
    "Return position of rightmost set bit for a binary number with N digits"
    return N-int(math.log2(n&-n))-1

cpdef int rotate_left(int i,int n,int N):
    "Rotate all bits to the left"
    return (n << i%N) & (2**N-1) |((n & (2**N-1)) >> (N-(i%N)))

cpdef rotate_right(int i,int n,int N):
    "Rotate all bits to the right"
    return ((n & (2**N-1)) >> i%N) |(n << (N-(i%N)) & (2**N-1))

cpdef get_even_bits(int n):
    "Count the number of set bits on even positions"
    return n & 0xAAAAAAAA

cpdef get_uneven_bits(int n):
    "Count the number of set bits on uneven positions"
    return n & 0x55555555

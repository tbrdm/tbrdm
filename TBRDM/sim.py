
""" sim.py
Author: Jonas Hachmeister
Last Change: 08.2020
About:
Main class that handles the calculation of the eigenvalues and eigestates to a given hamiltonian
and the calculation and testing of the 2-RDM

 """

import psutil #for memory measuring
import math
import time #for timing
import sys

from os import mkdir,rename #for file saving

import numpy as np
import scipy
import scipy.sparse.linalg
import scipy.stats
import matplotlib
import matplotlib.pyplot as plt
from pandas import DataFrame

import TBRDM.Hamiltonian as Hamiltonian
import TBRDM.Lanczos as Lanczos
from TBRDM.reduced_RDM import get_reduced_RDM
from TBRDM.RDM import get_RDM, get_OBRDM, get_full_RDM, get_RDM_index, construct_full_RDM,get_GRDM,rearrange_RDM


#for memory measuring
process = psutil.Process()

class Simulation:
    DEBUG_SHOW_TBRDM_EV =False
    DEBUG_TBRDM_TEST=False
    FLAG_NO_SAVE = 0 # 0 = save everything 1 = dont'save plots 2 = don't save anything
    FLAG_NO_OUTPUT = 2#0 = output everything 1 = don't output plots 2= no output
    FLAG_NO_ALERT = False
    FLAG_SHOW_MEMORY = True
    FLAG_FULL_RDM = False #If enabled the old routine to calculate the complete 2-RDM D^(2) will be used. Slower but wont need Cython
    FLAG_NO_OBRDM = False
    FLAG_NO_GRDM = False
    FLAG_NO_RRDM = False
    FLAG_C_RDM = True #use optimized RDM routine
    MAX_ITER = 50 #Maximum Iterations for Lanczos-Algorithm
    MAX_DELTA_E = 10**-5 #Maximum difference between Eigenvalues. Will stop if accuracy is reached
    MAX_DELTA_DEG = 0.001 #Maximum difference between lowest Eigenvalues. If difference is smaller the system seen as (quasi-)degenerated

    TBRDM_EV = 0
    G_TBRDM_EV = 0
    OBRDM_EV = 0
    DEGENERATED = False

    def set_debug(self,DEBUG_SHOW_TBRDM_EV,DEBUG_TBRDM_TEST):
        Simulation.DEBUG_SHOW_TBRDM_EV=DEBUG_SHOW_TBRDM_EV
        Simulation.DEBUG_TBRDM_TEST=DEBUG_TBRDM_TEST

    def set_flags(self,FLAG_NO_SAVE,FLAG_NO_OUTPUT,FLAG_NO_ALERT,FLAG_SHOW_MEMORY,FLAG_FULL_RDM,FLAG_NO_OBRDM,FLAG_NO_GRDM,FLAG_NO_RRDM,FLAG_C_RDM=True):
        Simulation.FLAG_NO_SAVE=FLAG_NO_SAVE
        Simulation.FLAG_NO_OUTPUT=FLAG_NO_OUTPUT
        Simulation.FLAG_NO_ALERT=FLAG_NO_ALERT
        Simulation.FLAG_SHOW_MEMORY = FLAG_SHOW_MEMORY
        Simulation.FLAG_FULL_RDM = FLAG_FULL_RDM
        Simulation.FLAG_NO_OBRDM = FLAG_NO_OBRDM
        Simulation.FLAG_NO_GRDM = FLAG_NO_GRDM
        Simulation.FLAG_NO_RRDM = FLAG_NO_RRDM
        Simulation.FLAG_C_RDM = FLAG_C_RDM

    def get_EV(self):
        "returns the eigenvalues of the 2-RDM"
        return Simulation.OBRDM_EV, Simulation.TBRDM_EV, Simulation.G_TBRDM_EV

    def get_Degenerated(self):
        "returns boolean which is true for a degenerated system"
        return Simulation.DEGENERATED

    def simulate(self,N, Nu, Nd, tx, ty, Vx, Vy, U, one_dim,DIR_NAME):

        DEBUG_SHOW_TBRDM_EV = Simulation.DEBUG_SHOW_TBRDM_EV
        DEBUG_TBRDM_TEST= Simulation.DEBUG_TBRDM_TEST
        FLAG_NO_SAVE = Simulation.FLAG_NO_SAVE # 0 = save everything 1 = dont'save plots 2 = don't save anything
        FLAG_NO_OUTPUT = Simulation.FLAG_NO_OUTPUT #0 = output everything 1 = don't output plots 2= no output
        FLAG_NO_ALERT = Simulation.FLAG_NO_ALERT
        FLAG_SHOW_MEMORY = Simulation.FLAG_SHOW_MEMORY
        FLAG_FULL_RDM = Simulation.FLAG_FULL_RDM
        FLAG_NO_OBRDM = Simulation.FLAG_NO_OBRDM
        FLAG_NO_GRDM = Simulation.FLAG_NO_GRDM
        FLAG_NO_RRDM = Simulation.FLAG_NO_RRDM
        FLAG_C_RDM = Simulation.FLAG_C_RDM
        MAX_ITER = Simulation.MAX_ITER #Maximum Iterations for Lanczos-Algorithm
        MAX_DELTA_E = Simulation.MAX_DELTA_E #Maximum difference between Eigenvalues. Will stop if accuracy is reached
        MAX_DELTA_DEG = Simulation.MAX_DELTA_DEG

        DIM = scipy.special.binom(N,Nu)*scipy.special.binom(N,Nd)

        start = time.time()
        PATH = DIR_NAME + "/"
        try:
            mkdir(PATH)

        except FileExistsError:
            pass

        print("Simulation parameters: \n N=",N, "Nu=", Nu, "Nd=", Nd, "one dimensional: ",one_dim, "\n tx=",tx, "ty=",ty, "Vx=",Vx, "Vy=",Vy, "U=",U,"\n saving to: ",PATH)
        print("Simulation parameters: \n N=",N, "\n Nu=", Nu, "\n Nd=", Nd, "\n one dimensional: ",one_dim, "\n tx=",tx, "\n ty=",ty, "\n Vx=",Vx, "\n Vy=",Vy, "\n U=",U,file=open(PATH+'info.txt','w'))
        print("---Simulation log---",file=open(PATH+'log.txt','w'))

        if Nu==0 or Nd==0:
            Nmax=N
        else:
            Nmax=2*N

        prelanczostime=time.time()
        if FLAG_SHOW_MEMORY==True:
            print("Memory usage:",process.memory_info()[0]/1000000,"mb")

        #Lanczos algorithm and EV calculation
        T,phi = Lanczos.lanczos(N,Nd,Nu,tx,ty,U,Vx,Vy,MAX_ITER,one_dim,MAX_DELTA_E,PATH)
        wt,vt = Lanczos.get_eigen(T,phi)

        lanczostime = time.time()
        Hdiagtime=lanczostime-prelanczostime
        print("Time for Diagonalization Alg:")
        print(Hdiagtime)


        idx = wt.argsort()[::1]  #sort eigenvalues by size
        wt=wt[idx]
        vt = vt[:,idx]

        ev_diff=np.abs(wt[0]-wt[1])
        #LOG
        print("Checking for degenerated groundstate. Difference between lowest Eigenvalues:", wt[0],"-",wt[1],"=",ev_diff,file=open(PATH+'log.txt','a'))
        #

        if ev_diff<MAX_DELTA_DEG:
            print("SYSTEM IS DEGENERATED")
            rename(DIR_NAME,DIR_NAME+'_DEG')
            PATH = DIR_NAME+"_DEG"+"/"
            DEGENERATED=True
            print("ALERT: System is degenerated!",file=open(PATH+'log.txt','a'))
        else:
            print("SYSTEM IS NOT DEGENERATED")

        if FLAG_NO_OUTPUT<2:
            print("Eigenvalues with Lanczos Alg:")
            print(DataFrame(wt))


            print("Eigenvectors with Lanczos Alg:")
            print(DataFrame(vt))


        if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
            x = np.arange(wt.shape[0])
            plt.figure()
            plt.plot(x,wt[x],'r.')
            plt.xlabel('Eigenvalues')
            plt.ylabel('Value')
            plt.title('Aproximation of the Eigenvalues of Two-Body Hamiltonian')

        if FLAG_NO_SAVE<2:
            np.savetxt(PATH+'eigenvalues.csv',wt,delimiter=',')
            np.savetxt(PATH+'eigenvectors.csv',vt[0],delimiter=',')
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'Hamilton_eigenvalues.png',dpi=200)
        if FLAG_SHOW_MEMORY==True:
            print("Memory usage:",process.memory_info()[0]/1000000,"mb")

        rdmtime = time.time()
        if FLAG_FULL_RDM==True:
            fRDM = get_full_RDM(vt[:,0],N,Nd,Nu)

            wcr, vcr = np.linalg.eig(RDM_cfull)
            idx = wcr.argsort()[::1]
            wcr = wcr[idx]


        else:
            if FLAG_C_RDM==True:
                fRDM = get_reduced_RDM(vt[:,0],N,Nd,Nu,PATH)
            else:
                fRDM = get_RDM(vt[:,0],N,Nd,Nu)
            RDM_cfull = construct_full_RDM(N,Nu,Nd,fRDM)
            rdmtime2 = time.time()
            rdmbuildtime=rdmtime2-rdmtime
            print("Time for RDM build: ",rdmbuildtime )


            wcr, vcr = np.linalg.eig(RDM_cfull)
            idx = wcr.argsort()[::1]
            wcr = wcr[idx]
            rdmtime3 = time.time()

            print("Time for RDM Diagonalization:",rdmtime3-rdmtime2)
        Simulation.TBRDM_EV = wcr

        if FLAG_SHOW_MEMORY==True:
            print("Memory usage:",process.memory_info()[0]/1000000,"mb")


        if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
            RDMfig=matplotlib.pyplot.matshow(np.real(fRDM))
            plt.colorbar(RDMfig)
            plt.xlabel('$[c_{i↑}c_{j↑}...c_{i↑}c_{j↓}]...[c_{i↓}c_{j↓}...c_{i↓}c_{j↑}]$')
            plt.ylabel('$[c_{k↑}^†c_{l↑}^†...c_{k↑}^†c_{l↓}^†]...[c_{k↓}^†c_{l↓}^†...c_{k↓}^†c_{l↑}]^†$')
            plt.title('2-RDM $<\psi | c_{i}^† c_{j}^† c_{k} c_{l} | \psi > $ for i<j and k>l',y=1.10)
        if FLAG_NO_SAVE==0:
            plt.savefig(PATH+'TBRDM.png',dpi=600)
            np.savetxt(PATH+'TBRDM.csv',fRDM,delimiter=',')
            np.savetxt(PATH+'TBRDM_eigenvalues.csv',wcr[::-1],delimiter=',')
            np.savetxt(PATH+'TBRDM_eigenstates.csv',vcr[::-1],delimiter=',')


        if FLAG_FULL_RDM==False:
            if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
                RDMfig=matplotlib.pyplot.matshow(np.real(RDM_cfull))
                plt.colorbar(RDMfig)
                plt.xlabel('$[c_{i↑}c_{j↑}...c_{i↑}c_{j↓}]...[c_{i↓}c_{j↑}...c_{i↓}c_{j↓}]$')
                plt.ylabel('$[c_{k↑}^†c_{l↑}^†...c_{k↑}^†c_{l↓}^†]...[c_{k↓}^†c_{l↑}^†...c_{k↓}^†c_{l↓}]^†$')
                plt.title('2-RDM $<\psi | c_{i}^† c_{j}^† c_{k} c_{l} | \psi > $',y=1.10)
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'TBRDM.png',dpi=600)
                np.savetxt(PATH+'TBRDM.csv',RDM_cfull,delimiter=',')
                np.savetxt(PATH+'TBRDM_eigenvalues.csv',wcr[::-1],delimiter=',')
                np.savetxt(PATH+'TBRDM_eigenstates.csv',vcr[::-1],delimiter=',')

        if DEBUG_SHOW_TBRDM_EV==1:
            print("Eigenvalues of RDM:")
            with pandas.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
                print(DataFrame(wcr))

        if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
            x = np.arange(wcr.shape[0])
            plt.figure()
            plt.plot(x,wcr[-x-1],'r.')
            plt.xlabel('Eigenvalues')
            plt.ylabel('Value')
            plt.title('Eigenvalues of 2-RDM')
        if FLAG_NO_SAVE==0:
            plt.savefig(PATH+'TBRDM_eigenvalues.png',dpi=200)

        if FLAG_FULL_RDM==False:
            if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
                x = np.arange(wcr.shape[0])
                plt.figure()
                plt.plot(x,wcr[-x-1],'r.')
                plt.xlabel('Eigenvalues')
                plt.ylabel('Value')
                plt.title('Eigenvalues of Full 2-RDM')
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'TBRDM_eigenvalues.png',dpi=200)

        #TESTS FOR TBRDM
        if DEBUG_TBRDM_TEST==True and FLAG_FULL_RDM==True:
            test=0
            for i in range(0,Nmax):
                for j in range(0,Nmax):
                    a=get_RDM_index(i,j,Nmax)
                    test=test+fRDM[a,a]

        if FLAG_FULL_RDM==True:
            print("Tests for full RDM")
            print("Tests for full RDM:",file=open(PATH+'log.txt','a'))
        else:
            print("Tests for reconstructed full RDM")
            print("Tests for reconstructed full RDM:",file=open(PATH+'log.txt','a'))

        print("Test 1: Trace= ", np.trace(RDM_cfull))


        print("Test 2: Sum of all EV = ", sum(wcr))

        print("Test 1: Sum of all EV = ", sum(wcr),"= Trace of RDM =",np.trace(RDM_cfull),file=open(PATH+'log.txt','a'))

        check= (Nd+Nu)*((Nd+Nu)-1)
        print("Check=",check)
        print("Check: (Nd+Nu)*((Nd+Nu)-1)=",check,file=open(PATH+'log.txt','a'))

        if abs(check-np.trace(RDM_cfull)>0.01):
            print("ERROR -> CHECK DIDN'T HOLD \a")
            print("ERROR: CHECK DIDN'T HOLD -> ABORTING SIMULATION",check,file=open(PATH+'log.txt','a'))
            print("VECTOR")
            print(DataFrame(vt[:,0]))
            sys.exit()




        #Create OBRDM from TBRDM
        if FLAG_NO_OBRDM==False:
            if FLAG_FULL_RDM==False:
                fRDM = RDM_cfull
            OBRDM=get_OBRDM(fRDM,N,Nd,Nu)
            print("Tests for 1-RDM")
            print("Trace of 1-RDM= ",np.trace(OBRDM))
            print("Check = ",Nu+Nd)
            print("Test for 1-RDM:\n","Test 1: Trace of 1-RDM =",np.trace(OBRDM),"\n Check: Nu+Nd=",Nu+Nd,file=open(PATH+'log.txt','a'))
            #print("Sum of OBRDM=",np.sum(OBRDM))
            owr, ovr = np.linalg.eig(OBRDM)
            idx = owr.argsort()[::1]
            ovr=ovr[idx]
            owr = owr[idx]
            Simulation.OBRDM_EV = owr
            # vt = vt[:,idx]
            if DEBUG_TBRDM_TEST==True:
                print("Test 3: Sum of EV from OBRDM =",sum(owr), "Check=",Nd+Nu)


            if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
                OBRDMfig=matplotlib.pyplot.matshow(np.real(OBRDM))
                plt.colorbar(OBRDMfig)
                plt.xlabel('$c_{i}^† $ ')
                plt.ylabel('$c_j$')
                plt.title('1-RDM of the lattice')
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'OBRDM.png',dpi=300)
                np.savetxt(PATH+'OBRDM.csv',OBRDM,delimiter=',')
                np.savetxt(PATH+'OBRDM_eigenvalues.csv',owr,delimiter=',')

        #CREATE GRDM
        if FLAG_NO_GRDM==False and FLAG_NO_OBRDM==False:
            GRDM = get_GRDM(fRDM,OBRDM,N,Nu,Nd)
            print("Tests for GRDM")
            print("Trace of GRDM= ",np.trace(GRDM))
            #print("Sum of GRDM=",np.sum(GRDM))
            gwr, gvr = np.linalg.eig(GRDM)
            idx = gwr.argsort()[::1]
            gwr = gwr[idx]
            Simulation.G_TBRDM_EV=gwr
            gvr=gvr[idx]
            #print("Eigenvalues of GRDM")
            #print(DataFrame(gwr))
            print("Sum of EV=",np.sum(gwr))
            print("Check=",(Nu+Nd)*(Nmax-(Nu+Nd)+1))
            print("Test for 2-RDM G:\n","Test 1: Trace of G=",np.trace(GRDM),"=Sum of EV=",np.sum(gwr),"\n Check: (Nu+Nd)*(Nmax-(Nu+Nd)+1)=",(Nu+Nd)*(Nmax-(Nu+Nd)+1),file=open(PATH+'log.txt','a'))
            tst=0

            for i in range(0,Nmax):
                g1=get_RDM_index(i,i,Nmax)
                g2=get_RDM_index(2,3,Nmax)
                tst=tst+GRDM[g1,g2]
            print("Additional test with OBRDM: ",tst)

            print("Check=",(Nu+Nd)*OBRDM[2,3])
            print("Additional Test for 1-RDM and 2-RDMG:\n","Test=",tst,"\n Check=",(Nu+Nd)*OBRDM[2,3],file=open(PATH+'log.txt','a'))

            if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
                GRDMfig=matplotlib.pyplot.matshow(np.real(GRDM))
                plt.colorbar(GRDMfig)
                plt.xlabel('$[c_{i↑}^†c_{j↑}...c_{i↑}^†c_{j↓}]...[c_{i↓}^†c_{j↑}...c_{i↓}^†c_{j↓}]$')
                plt.ylabel('$[c_{k↑}^†c_{l↑}...c_{k↑}^†c_{l↓}]...[c_{k↓}^†c_{l↑}...c_{k↓}^†c_{l↓}]$')
                plt.title('2-RDM $<\psi | c_{i}^† c_{j} c_{k}^† c_{l} | \psi > $ of the lattice',y=1.10)
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'G_TBRDM.png',dpi=300)
                np.savetxt(PATH+'G_TBRDM.csv',GRDM,delimiter=',')
                np.savetxt(PATH+'G_TBRDM_eigenvalues.csv',gwr[::-1],delimiter=',')
                np.savetxt(PATH+'G_TBRDM_eigenstates.csv',gvr[::-1],delimiter=',')
            if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
                x = np.arange(gwr.shape[0])
                plt.figure()
                plt.plot(x,gwr[-x-1],'r.')
                plt.xlabel('Eigenvalues')
                plt.ylabel('Value')
                plt.title('Eigenvalues of Full 2-RDM G')
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'G_TBRDM_eigenvalues.png',dpi=200)



        #####
        if FLAG_NO_RRDM==False:
            rearranged_RDM = rearrange_RDM(fRDM,N,Nu,Nd)
            print("Trace of rearranged RDM =",np.trace(rearranged_RDM))
            if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
                RRDMfig=matplotlib.pyplot.matshow(np.real(rearranged_RDM))
                plt.colorbar(RRDMfig)
                plt.xlabel('$c_{i↑}c_{j↑}...c_{i↓}c_{j↓}...c_{i↑}c_{j↓}...c_{i↓}c_{j↑}$')
                plt.ylabel('$c_{k↑}^†c_{l↑}^†...c_{k↓}^†c_{l↓}^†...c_{k↑}^†c_{l↓}^†...c_{k↓}^†c_{l↑}^†$')
                plt.title('rearranged 2-RDM $<\psi | c_{i}^† c_{j}^† c_{k} c_{l} | \psi > $',y=1.10)
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'R_TBRDM.png',dpi=300)

        if FLAG_NO_RRDM==False and FLAG_NO_GRDM==False:
            rearranged_GRDM = rearrange_RDM(GRDM,N,Nu,Nd)
            print("Trace of rearranged GRDM =",np.trace(rearranged_GRDM))
            if FLAG_NO_OUTPUT==0 or FLAG_NO_SAVE==0:
                GRRDMfig=matplotlib.pyplot.matshow(np.real(rearranged_GRDM))
                plt.colorbar(GRRDMfig)
                plt.xlabel('$c_{i↑}^†c_{j↑}...c_{i↓}^†c_{j↓}...c_{i↑}^†c_{j↓}...c_{i↓}^†c_{j↑}$')
                plt.ylabel('$c_{k↑}^†c_{l↑}...c_{k↓}^†c_{l↓}...c_{k↑}^†c_{l↓}...c_{k↓}^†c_{l↑}$')
                plt.title('rearranged 2-RDM $<\psi | c_{i}^† c_{j} c_{k}^† c_{l} | \psi > $',y=1.10)
            if FLAG_NO_SAVE==0:
                plt.savefig(PATH+'R_G_TBRDM.png',dpi=300)
        ###

        ###calculate von-neumann-entropie for the 2-RDM2
        #S_TBRMD_D = -np.sum(wcr*np.log(wcr))
        S_TBRMD_D=0
        #for e in wcr:
        #    S_TBRMD_D+=e*np.log(e)
        #    print(e,S_TBRMD_D)
        #S_TBRMD_D=-1*S_TBRMD_D
        S_TBRDM_D = scipy.stats.entropy(np.round(np.real(wcr/sum(wcr)),10))
        S_TBRDM_G = scipy.stats.entropy(np.round(np.real(gwr/sum(gwr)),10))
        S_OBRDM_D = scipy.stats.entropy(np.round(np.real(owr/sum(owr)),10))
        print("Von Neumann Entropy: ")
        print("For 2-RDM D: ",S_TBRDM_D)
        print("For 2-RDM G: ",S_TBRDM_G)
        print("For 1-RDM D:",S_OBRDM_D)
        print("Von Neumann Entropy\n",S_TBRDM_D, "\n",S_TBRDM_G, "\n",S_OBRDM_D,file=open(PATH+'entropy.csv','w'))

        N_s=N
        N_p=Nu+Nd
        if Nu!=0 and Nd!=0:
            N_s=2*N

        E1=math.log(N_p)
        E2=math.log(N_p*(N_p-1)/2)
        E3=math.log(N_s-N_p+1)/(N_s-N_p+1)+(N_s-N_p)*math.log(N_p*(N_s-N_p+1))/(N_s-N_p+1)
        ME1=math.log(N_s)
        ME2=math.log(N_s*(N_s-1)/2)
        ME3=2*math.log(N_s)

        print("\nTest for Entropy of the RDM:\n",file=open(PATH+'log.txt','a'))
        print("Entropy of 2-RDM D =",S_TBRDM_D,"<=",ME2,file=open(PATH+'log.txt','a'))
        print("Entropy of 2-RDM G =",S_TBRDM_G,"<=",ME3,file=open(PATH+'log.txt','a'))
        print("Entropy of 1-RDM D =",S_OBRDM_D,"<=",ME1,file=open(PATH+'log.txt','a'))
        if U==0 and Vx==0 and Vy==0:
            print("System seems to be Slater-Determinant. Entropies should equal to: \n",file=open(PATH+'log.txt','a'))
            print("Entropy of 2-RDM D =",E2,file=open(PATH+'log.txt','a'))
            print("Entropy of 2-RDM G =",E3,file=open(PATH+'log.txt','a'))
            print("Entropy of 1-RDM D =",E1,file=open(PATH+'log.txt','a'))

        end = time.time()
        print("Runtime:")
        print(end-start)

        #Performance log
        print("Runtime:\n",end-start,"\n Hamiltonoperator diagonalization time: \n",Hdiagtime,"\n RDM-Buildtime:\n",rdmbuildtime,"\n Memory:\n",process.memory_info()[0]/1000000,"mb",file=open(PATH+'performance.txt','w'))

        if FLAG_NO_OUTPUT==0:
            matplotlib.pyplot.show()
        matplotlib.pyplot.close('all')
        if FLAG_NO_ALERT==False:
                print('\a')

# cython: language_level=3, boundscheck=False,initializedcheck=False,cdivision=True, nonecheck=False


""" reduced_RDM.pyx
Author: Jonas Hachmeister
Last Change: 08.2020
About:
Handles the calculation of the reduced 2-RDM D_a(i,j;k,l)

 """

import numpy as np
cimport numpy as np

import scipy.special

import TBRDM.Hamiltonian as Hm
import TBRDM.bithacks as bit

#The possible states of the system are saved globally to be called using a dicitionary
cdef tuple statesu
cdef tuple statesd
cdef dict statesu_dict
cdef dict statesd_dict

cdef tuple get_xy(int i,int Nsd):
    "Up- und Down-Zustand zu Gesamtzustand bekommen"
    cdef int x,y
    y = i%(Nsd)
    x =int((i-y)/(Nsd))
    return x,y

cdef int get_index(int x,int y,int N,int Ns):
    "Gesamtzustand für Up- und Down-Zustand bekommen"
    return x*scipy.special.binom(N,Ns)+y


def get_reduced_RDM(np.ndarray v,int N,int Nd,int Nu, str PATH):
    "Berechnung der Untermatrix der reduzierten Dichtematrix D^(2)"
    global statesu
    global statesd
    global statesu_dict
    global statesd_dict

    cdef int Nsu = int(scipy.special.binom(N,Nu))
    cdef int Nsd = int(scipy.special.binom(N,Nd))
    cdef int Ns = Nsu*Nsd #Dimension of the hilbertspace

    cdef int i,j,l,k,z,a,b,Nmax
    cdef complex value

    #The basis is a tuple of binary numbers
    #A dicitionary is generated for O(1) access
    statesu = Hm.get_binary_states_bin(N,Nu)
    statesu_dict = dict((e,i) for i,e in enumerate(statesu))
    statesd = Hm.get_binary_states_bin(N,Nd)
    statesd_dict = dict((e,i) for i,e in enumerate(statesd))

    print("---Starting reduced RDM calculation---",file=open(PATH+'log.txt','a'))

    #Initialisation of the output matrix
    if Nu==0 or Nd==0:
        RDM = np.zeros((int(N/2*(N-1)),int(N/2*(N-1))),dtype=np.complex_)
        Nmax=N
    else:
        RDM = np.zeros((N*(2*N-1),N*(2*N-1)),dtype=np.complex_)
        Nmax=2*N

    cdef complex[:,:] RDM_view = RDM

    #Calculation of the 2-RDM
    a=-1 #Index for output matrix
    for i in range(0,Nmax):
        print("Progress: ",i," / ",Nmax,file=open(PATH+'log.txt','a'))
        for j in range(0,i):
            a=a+1
            b=-1 #Index for output matrix
            if i==j:    #Early test wether A=0
                continue
            for l in range(0,Nmax):
                for k in range(0,l):
                    b=b+1
                    if k==l:    #Index for output matrix
                        continue
                    value = 0

                    #Index for output matrix
                    if (i==j and j==k and k==l) or (i<N and j<N and (l>=N or k>=N)) or (i>=N and j>=N and (l<N or k<N)):
                        continue
                    elif (i>=N and (j<N and k<N and l<N)) or (j>=N and (i<N and k<N and l<N)) or (k>=N and (j<N and i<N and l<N)) or (l>=N and (j<N and k<N and i<N)):
                        continue
                    elif (i<N and (j>=N and k>=N and l>=N)) or (j<N and (i>=N and k>=N and l>=N)) or (k<N and (j>=N and i>=N and l>=N)) or (l<N and (j>=N and k>=N and i>=N)):
                        continue

                    #Inner loop over the whole basis
                    for z in range(0,Ns):
                        x,y = get_xy(z,Nsd)
                        value=value+calc(z,i,j,k,l,x,y,N,Nd,v)



                    RDM_view[a,b]=value


    return RDM

#Calculation of inner loop
cdef complex calc(int z, int i, int j, int k, int l, int x, int y,int N,int Nd,np.ndarray v):
    cdef int  A, bx, by , bi, n
    cdef int temp_u, temp_d, temp_u2, temp_d2
    temp_u = statesu[x]
    temp_d = statesd[y]
    temp_u2 =statesu[x]
    temp_d2 = statesd[y]

    A=0
    #Here the creation- and annihilation operators are applied on the state to determine |A|
    if l<N:
        A=bit.get_i_bit(l,temp_u2,N)
        temp_u2=bit.flip_i_bit(l,temp_u2,N)
    else:
        A=bit.get_i_bit(l-N,temp_d2,N)
        temp_d2=bit.flip_i_bit(l-N,temp_d2,N)
    if A==0:
        return 0
    if k<N:
        A=A*bit.get_i_bit(k,temp_u2,N)
        temp_u2=bit.flip_i_bit(k,temp_u2,N)
    else:
        A=A*bit.get_i_bit(k-N,temp_d2,N)
        temp_d2=bit.flip_i_bit(k-N,temp_d2,N)
    if A==0:
        return 0
    if j<N:
        A=A*(1-bit.get_i_bit(j,temp_u2,N))
        temp_u2=bit.flip_i_bit(j,temp_u2,N)
    else:
        A=A*(1-bit.get_i_bit(j-N,temp_d2,N))
        temp_d2=bit.flip_i_bit(j-N,temp_d2,N)
    if A==0:
        return 0
    if i<N:
        A=A*(1-bit.get_i_bit(i,temp_u2,N))
        temp_u2=bit.flip_i_bit(i,temp_u2,N)
    else:
        A=A*(1-bit.get_i_bit(i-N,temp_d2,N))
        temp_d2=bit.flip_i_bit(i-N,temp_d2,N)
    if A==0:
        return 0

    #Calculate sign of A
    if A==1:
        n=0
        if l<N:
            temp_u=bit.flip_i_bit(l,temp_u,N)
            n=bit.countSetBits(temp_u>>(N-1-l))
        else:
            temp_d=bit.flip_i_bit(l-N,temp_d,N)
            n+=bit.countSetBits(temp_u)
            n+=bit.countSetBits(temp_d>>(N-1-(l-N)))
        if k<N:
            temp_u=bit.flip_i_bit(k,temp_u,N)
            n+=bit.countSetBits(temp_u>>(N-1-k))
        else:
            temp_d=bit.flip_i_bit(k-N,temp_d,N)
            n+=bit.countSetBits(temp_u)
            n+=bit.countSetBits(temp_d>>(N-1-(k-N)))
        if j<N:
            n+=bit.countSetBits(temp_u>>(N-1-j))
            temp_u=bit.flip_i_bit(j,temp_u,N)
        else:
            n+=bit.countSetBits(temp_u)
            n+=bit.countSetBits(temp_d>>(N-1-(j-N)))
            temp_d=bit.flip_i_bit(j-N,temp_d,N)
        if i<N:
            n+=bit.countSetBits(temp_u>>(N-1-i))
            temp_u=bit.flip_i_bit(i,temp_u,N)
        else:
            n+=bit.countSetBits(temp_u)
            n+=bit.countSetBits(temp_d>>(N-1-(i-N)))
            temp_d=bit.flip_i_bit(i-N,temp_d,N)
        A=(-1)**n
        bx = statesu_dict[temp_u]
        by = statesd_dict[temp_d]
        bi = get_index(bx,by,N,Nd)
        return A*np.conj(v[bi])*v[z]
    return 0


""" reduced_RDM.pyx
Author: Jonas Hachmeister
Last Change: 08.2020
About:
Handles the calculation the calculation of the full 2-RDM D^(2) and the construction
of the 1-RDM D^(1) and 2-RDM G^(2)

 """


import numpy as np
import scipy.special

import TBRDM.Hamiltonian as Hm


def get_RDM_index(x,y,Ns):
    return x*Ns+y

def get_full_binary_states(N):
    states = np.zeros((1,N))
    for x in range(1,N):
        states = np.append(states,Hm.get_binary_states(N,x),axis=0)

    states = np.append(states,np.ones((1,N)),axis=0)
    return states


def get_OBRDM(RDM,N,Nd,Nu):
    "Constructs 1-RDM D^(1) from 2-RDM D^(2)"
    if Nu==0 or Nd==0:
        OBRDM = np.zeros(((N),(N)),dtype=np.complex_)
        Nmax=N
    else:
        OBRDM = np.zeros(((2*N),(2*N)),dtype=np.complex_)
        Nmax=2*N
    if Nu+Nd-1 ==0:
        return OBRDM
    for i in range(0,Nmax):
        for j in range(0,Nmax):
            for l in range(0,Nmax):
                a=get_RDM_index(i,l,Nmax)
                b=get_RDM_index(j,l,Nmax)
                OBRDM[i,j]=OBRDM[i,j]+RDM[a,b]
            OBRDM[i,j]=OBRDM[i,j]/(Nu+Nd-1)
    return OBRDM


def get_RDM(v,N,Nd,Nu):
    "Old method to calculate the 2-RDM D_a^(2). Not optimized and will just be used if FLAG_C_RDM=False"
    Nsu = int(scipy.special.binom(N,Nu))
    Nsd = int(scipy.special.binom(N,Nd))
    statesu = Hm.get_binary_states(N,Nu)
    statesd = Hm.get_binary_states(N,Nd)
    Ns = int(Hm.get_index(Nsu-1,Nsd-1,N,Nd))+1

    if Nu==0 or Nd==0:
        RDM = np.zeros((int(N/2*(N-1)),int(N/2*(N-1))),dtype=np.complex_)
    else:
        RDM = np.zeros((N*(2*N-1),N*(2*N-1)),dtype=np.complex_)

    indexlist = np.zeros((Ns,2), dtype = int)

    for z in range(0,Ns):
        indexlist[z]=Hm.get_xy(z,Nsu,Nsd)

    hlp=N-1
    a=-1
    if Nu==0 or Nd==0:
        Nmax=N
    else:
        Nmax=2*N
    for i in range(0,Nmax):

        for j in range(0,i):
            a=a+1
            b2=-1
            for l in range(0,Nmax):

                for k in range(0,l):
                    b2=b2+1

                    value = 0
                    for z in range(0,Ns):
                        x,y = indexlist[z]
                        x = int(x)
                        y = int(y)
                        temp_u = statesu[x][:]
                        temp_d = statesd[y][:]
                        temp_u2 =statesu[x][:]
                        temp_d2 = statesd[y][:]


                        A=0
                        if (i==j and j==k and k==l) or (i==j) or (k==l) or (i<N and j<N and (l>=N or k>=N)) or (i>=N and j>=N and (l<N or k<N)):
                            A=0
                        elif (i>=N and (j<N and k<N and l<N)) or (j>=N and (i<N and k<N and l<N)) or (k>=N and (j<N and i<N and l<N)) or (l>=N and (j<N and k<N and i<N)):
                            A=0
                        elif (i<N and (j>=N and k>=N and l>=N)) or (j<N and (i>=N and k>=N and l>=N)) or (k<N and (j>=N and i>=N and l>=N)) or (l<N and (j>=N and k>=N and i>=N)):
                            A=0
                        else:
                            if l<N:
                                A=temp_u2[l]
                                temp_u2[l]=0
                            else:
                                A=temp_d2[l-N]
                                temp_d2[l-N]=0
                            if A!=0:
                                if k<N:
                                    A=A*temp_u2[k]
                                    temp_u2[k]=0
                                else:
                                    A=A*temp_d2[k-N]
                                    temp_d2[k-N]=0
                            if A!=0:
                                if j<N:
                                    A=A*(1-temp_u2[j])
                                    temp_u2[j]=1
                                else:
                                    A=A*(1-temp_d2[j-N])
                                    temp_d2[j-N]=1
                            if A!=0:
                                if i<N:
                                    A=A*(1-temp_u2[i])
                                    temp_u2[i]=1
                                else:
                                    A=A*(1-temp_d2[i-N])
                                    temp_d2[i-N]=1

                        if A==1:

                            if 1==1:

                                if l<N:

                                    temp_u[l]=0

                                    for zz in range(0,l):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                else:
                                    temp_d[l-N]=0

                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,l-N):
                                        if temp_d[zz]==1:
                                            A=A*-1


                                if k<N:

                                    temp_u[k]=0
                                    for zz in range(0,k):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                else:
                                    temp_d[k-N]=0
                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,k-N):
                                        if temp_d[zz]==1:
                                            A=A*-1

                                if j<N:

                                    for zz in range(0,j):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    temp_u[j]=1
                                else:
                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,j-N):
                                        if temp_d[zz]==1:
                                            A=A*-1
                                    temp_d[j-N]=1

                                if i<N:

                                    for zz in range(0,i):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    temp_u[i]=1
                                else:
                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,i-N):
                                        if temp_d[zz]==1:
                                            A=A*-1
                                    temp_d[i-N]=1



                            bx = statesu.index(temp_u)
                            by = statesd.index(temp_d)
                            bi = Hm.get_index(bx,by,N,Nd)

                            bi = int(bi)
                            z = int(z)

                            value = value+A*np.conj(v[bi])*v[z]
                    RDM[a,b2]=value

    return RDM

def construct_full_RDM(N,Nu,Nd,RDM):
    "Construct full 2-RDM D^(2) from D_a^(2)"
    if Nu==0 or Nd==0:
        out = np.zeros(((N)**2,(N)**2),dtype=np.complex_)
        Nmax=N
    else:
        Nmax=2*N
        out = np.zeros(((2*N)**2,(2*N)**2),dtype=np.complex_)

    a=-1
    for i in range(0,Nmax):

        for j in range(0,i):
            a=a+1
            b=-1
            for l in range(0,Nmax):
                for k in range(0,l):
                    b=b+1
                    a2=get_RDM_index(i,j,Nmax)
                    a3=get_RDM_index(j,i,Nmax)
                    b2=get_RDM_index(l,k,Nmax)
                    b3=get_RDM_index(k,l,Nmax)
                    out[a2,b2]=RDM[a,b]
                    out[a3,b2]=-RDM[a,b]
                    out[a2,b3]=-RDM[a,b]
                    out[a3,b3]=RDM[a,b]
    return out




def get_full_RDM(v,N,Nd,Nu):
    "Old method to calculate the 2-RDM D^(2). Not optimized and will just be used if FLAG_FULL_RDM=True"
    Nsu = int(scipy.special.binom(N,Nu))
    Nsd = int(scipy.special.binom(N,Nd))
    statesu = Hm.get_binary_states(N,Nu)
    statesd = Hm.get_binary_states(N,Nd)
    Ns = int(Hm.get_index(Nsu-1,Nsd-1,N,Nd))+1

    if Nu==0 or Nd==0:
        RDM = np.zeros(((N)**2,(N)**2),dtype=np.complex_)
        Nmax=N
    else:
        Nmax=2*N
        RDM = np.zeros(((2*N)**2,(2*N)**2),dtype=np.complex_)

    #assert v.dtype == np.ndarray

    indexlist = np.zeros((Ns,2), dtype = int)

    for z in range(0,Ns):
        indexlist[z]=Hm.get_xy(z,Nsu,Nsd)



    for i in range(0,Nmax):
        for j in range(0,Nmax):
            for k in range(0,Nmax):
                for l in range(0,Nmax):
                    a=get_RDM_index(i,j,Nmax)

                    b2=get_RDM_index(l,k,Nmax)

                    value = 0
                    for z in range(0,Ns):
                        x,y = indexlist[z]
                        x = int(x)
                        y = int(y)
                        temp_u = statesu[x][:]
                        temp_d = statesd[y][:]
                        temp_u2 =statesu[x][:]
                        temp_d2 = statesd[y][:]


                        A=0
                        if (i==j and j==k and k==l) or (i==j) or (k==l) or (i<N and j<N and (l>=N or k>=N)) or (i>=N and j>=N and (l<N or k<N)):
                            A=0
                        elif (i>=N and (j<N and k<N and l<N)) or (j>=N and (i<N and k<N and l<N)) or (k>=N and (j<N and i<N and l<N)) or (l>=N and (j<N and k<N and i<N)):
                            A=0
                        elif (i<N and (j>=N and k>=N and l>=N)) or (j<N and (i>=N and k>=N and l>=N)) or (k<N and (j>=N and i>=N and l>=N)) or (l<N and (j>=N and k>=N and i>=N)):
                            A=0
                        else:
                            if l<N:
                                A=temp_u2[l]
                                temp_u2[l]=0
                            else:
                                A=temp_d2[l-N]
                                temp_d2[l-N]=0
                            if A!=0:
                                if k<N:
                                    A=A*temp_u2[k]
                                    temp_u2[k]=0
                                else:
                                    A=A*temp_d2[k-N]
                                    temp_d2[k-N]=0
                            if A!=0:
                                if j<N:
                                    A=A*(1-temp_u2[j])
                                    temp_u2[j]=1
                                else:
                                    A=A*(1-temp_d2[j-N])
                                    temp_d2[j-N]=1
                            if A!=0:
                                if i<N:
                                    A=A*(1-temp_u2[i])
                                    temp_u2[i]=1
                                else:
                                    A=A*(1-temp_d2[i-N])
                                    temp_d2[i-N]=1




                        if A==1:

                            if 1==1:

                                if l<N:

                                    temp_u[l]=0

                                    for zz in range(0,l):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                else:
                                    temp_d[l-N]=0

                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,l-N):
                                        if temp_d[zz]==1:
                                            A=A*-1

                                if k<N:

                                    temp_u[k]=0
                                    for zz in range(0,k):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                else:
                                    temp_d[k-N]=0
                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,k-N):
                                        if temp_d[zz]==1:
                                            A=A*-1


                                if j<N:

                                    for zz in range(0,j):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    temp_u[j]=1
                                else:
                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,j-N):
                                        if temp_d[zz]==1:
                                            A=A*-1
                                    temp_d[j-N]=1
                                if i<N:

                                    for zz in range(0,i):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    temp_u[i]=1
                                else:
                                    for zz in range(0,N):
                                        if temp_u[zz]==1:
                                            A=A*-1
                                    for zz in range(0,i-N):
                                        if temp_d[zz]==1:
                                            A=A*-1
                                    temp_d[i-N]=1

                            bx = statesu.index(temp_u)
                            by = statesd.index(temp_d)
                            bi = Hm.get_index(bx,by,N,Nd)

                            bi = int(bi)
                            z = int(z)

                            value = value+A*np.conj(v[bi])*v[z]

                    RDM[a,b2]=value

    return RDM


def get_GRDM(full_RDM,OBRDM,N,Nu,Nd):
    "Constructs 2-RDM G^(2) from 2-RDM D^(2)"
    if Nu==0 or Nd==0:
        GRDM = np.zeros(((N)**2,(N)**2),dtype=np.complex_)
        Nmax=N
    else:
        GRDM = np.zeros(((2*N)**2,(2*N)**2),dtype=np.complex_)
        Nmax=2*N

    for i in range(0,Nmax):
        for j in range(0,Nmax):
            for k in range(0,Nmax):
                for l in range(0,Nmax):
                    r1=get_RDM_index(i,k,Nmax)
                    r2=get_RDM_index(l,j,Nmax)

                    g1=get_RDM_index(i,j,Nmax)
                    g2=get_RDM_index(l,k,Nmax)

                    # r1=get_RDM_index(i,k,2*N)
                    # r2=get_RDM_index(l,j,2*N)
                    #
                    # g1=get_RDM_index(i,l,2*N)
                    # g2=get_RDM_index(j,k,2*N)
                    if j==k:
                        GRDM[g1,g2]=OBRDM[i,l] -full_RDM[r1,r2]
                    else:
                        GRDM[g1,g2] = -full_RDM[r1,r2]

    return GRDM

def rearrange_RDM(full_RDM,N,Nu,Nd):
    "Rearranges the indices of the 2-RDM D^(2). Deprecated and not used."
    if Nu==0 or Nd==0:
        out= np.zeros(((N)**2,(N)**2),dtype=np.complex_)
        Nmax=N
    else:
        out= np.zeros(((2*N)**2,(2*N)**2),dtype=np.complex_)
        Nmax=2*N

    for i in range(0,Nmax):
        for j in range(0,Nmax):
            for k in range(0,Nmax):
                for l in range(0,Nmax):
                    r1=get_RDM_index(i,j,Nmax)
                    r2=get_RDM_index(l,k,Nmax)
                    i1=0
                    i2=0
                    if i<N and j<N:
                        i1=r1
                    if l<N and k<N:
                        i2=r2
                    if i>=N and j>=N:
                        i1=get_RDM_index(i-N,j,Nmax)
                    if l>=N and k>=N:
                        i2=get_RDM_index(l-N,k,Nmax)
                    if i<N and j>=N:
                        i1=get_RDM_index(i+N,j-N,Nmax)
                    if l<N and k>=N:
                        i2=get_RDM_index(l+N,k-N,Nmax)
                    if i>=N and j<N:
                        i1=get_RDM_index(i,j+N,Nmax)
                    if l>=N and k<N:
                        i2=get_RDM_index(l,k+N,Nmax)

                    out[i1,i2]=full_RDM[r1,r2]


    return out

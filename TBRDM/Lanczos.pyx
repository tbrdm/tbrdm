# cython: language_level=3, boundscheck=False,initializedcheck=False,cdivision=True, nonecheck=False

""" Lanczos.pyx
Author: Jonas Hachmeister
Last Change: 08.2020
About:
Handles the calculation of the extremal eigenvalues and eigenstates for the given system

 """

import time
import sys
import itertools

cimport numpy as np
import numpy as np
import scipy
import scipy.sparse as sp

import TBRDM.Hamiltonian as Hm

DTYPE = np.float32

cpdef np.ndarray make_rand_vec(int dim):
    "Returns a random n-dim normalized vector"
    v = np.random.rand(dim)
    return v/np.linalg.norm(v)

cpdef get_eigen(T,phi,vectors=True):
    w, v = np.linalg.eig(T)
    if vectors==True:
        vt = phi@v
    else:
        vt=0
    return w,vt

cpdef np.ndarray calculate_ws(np.ndarray v, int N, int Nu, int Nd, float tx, float ty, float U, float Vx, float Vy, bint one_dim,int Nsu, int Nsd, int dim, tuple statesu, tuple statesd, dict statesu_dict, dict statesd_dict):
    "Calculation of the lanczos vector ws which is needed for the lanczos method"
    ws = np.zeros(dim,dtype=DTYPE)
    cdef float[:] ws_view=ws
    cdef float[:] v_view =v
    cdef int z,i,j,x2,y2, x, y
    cdef int A
    cdef int up_state, down_state, new_state
    for z in range(0,dim):
        x,y = Hm.get_xy(z,Nsd)
        #apply non diag terms
        up_state = statesu[x]
        down_state = statesd[y]
        for i in range(0,N):

            if Nu>0:
                if one_dim==True:
                    A,new_state = Hm.apply_non_diag_term_ring(up_state,i,N,Nu) #for up-spin
                else:
                    A,new_state = Hm.apply_non_diag_term_chain_vert(up_state,i,N,Nu) #for up-spin
                    if A!=0:
                        x2=statesu_dict[new_state]
                        j=Hm.get_index(x2,y,N,Nd)
                        ws_view[j]=ws_view[j]+A*ty*v_view[z]

                    A,new_state = Hm.apply_non_diag_term_chain_horiz(up_state,i,N,Nu) #for up-spin
                if A!=0:
                    x2=statesu_dict[new_state]
                    j=Hm.get_index(x2,y,N,Nd)
                    ws_view[j]=ws_view[j]+A*tx*v_view[z]
            if Nd>0:
                if one_dim==True:
                    A,new_state = Hm.apply_non_diag_term_ring(down_state,i,N,Nd) #for down-spin
                else:
                    A,new_state = Hm.apply_non_diag_term_chain_vert(down_state,i,N,Nd) #for down-spin
                    if A!=0:
                        y2=statesd_dict[new_state]
                        j=Hm.get_index(x,y2,N,Nd)
                        ws_view[j]=ws_view[j]+A*ty*v_view[z]

                    A,new_state = Hm.apply_non_diag_term_chain_horiz(down_state,i,N,Nd) #for down-spin

                if A!=0:
                    y2=statesd_dict[new_state]
                    j=Hm.get_index(x,y2,N,Nd)
                    ws_view[j]=ws_view[j]+A*tx*v_view[z]

        if one_dim==True:
            ws_view[z]=ws_view[z]+Hm.get_diag_term_ring(up_state,down_state,N,U,Vx,Vy)*v_view[z]
        else:
            ws_view[z]=ws_view[z]+Hm.get_diag_term_chain(up_state,down_state,N,U,Vx,Vy)*v_view[z]
    return ws


def lanczos(int N,int Nd,int Nu,float tx,float ty,float U,float Vx,float Vy,int n,bint one_dim,float MAX_DELTA_E,str PATH):
    "Calculate the n most useful eigenvalues and vectors for the Matrix m"
    cdef int Nsu = int(scipy.special.binom(N,Nu))
    cdef int Nsd = int(scipy.special.binom(N,Nd))
    cdef int dim = Nsu*Nsd
    print("DIMENSION OF SYSTEM:",dim)
    cdef tuple statesu = Hm.get_binary_states_bin(N,Nu)
    cdef tuple statesd = Hm.get_binary_states_bin(N,Nd)
    cdef dict statesu_dict = dict((e,i) for i,e in enumerate(statesu))
    cdef dict statesd_dict = dict((e,i) for i,e in enumerate(statesd))


    T = np.zeros((n,n),dtype=DTYPE)
    cdef float[:,:] T_view = T
    v = np.array(make_rand_vec(dim),dtype=DTYPE)
    cdef float[:] v_view = v

    vl =np.zeros((dim),dtype=DTYPE)
    cdef float[:] vl_view =vl

    p = np.zeros(dim,dtype=DTYPE)

    vo = np.zeros((dim,n),dtype=DTYPE)
    cdef float[:,:] vo_view = vo

    vo_view[:,0]=v_view

    ws = np.zeros(dim,dtype=DTYPE)
    cdef float[:] ws_view = ws

    #a, b will be the coefficients of the tridiagonal matrix
    a = np.zeros(dim,dtype=DTYPE)
    b = np.zeros(dim,dtype=DTYPE)
    #ws = M.dot(v)
    cdef Py_ssize_t i,j,x
    cdef float temp
    temp2 = np.zeros(dim,dtype=DTYPE)

    wt = np.zeros(n,dtype=DTYPE)
    vt = np.zeros((n,n),dtype=DTYPE)
    cdef float wto_0, wto_1
    cdef float diff_0, diff_1
    wto_0 = wto_1= -9999

    cdef float tmean,t_1,t_2
    tmean=0


    print("---Beginning Lanczos Procedure---",file=open(PATH+'log.txt','a'))
    ws=calculate_ws(v, N, Nu, Nd, tx, ty,U, Vx, Vy, one_dim, Nsu, Nsd, dim, statesu, statesd, statesu_dict, statesd_dict)


    a[0]=ws@v
    ws = ws - a[0]*v
    T[0,0]=a[0]

    out_size=0
    for x in range(1,n):
        out_size=x

        b[x] = np.linalg.norm(ws)
        if b[x]!=0:
            vl = v
            v = ws/b[x]
            vo[:,x]=v

            #reorthonormalization
            if x>1:
                p = np.zeros(dim,dtype=DTYPE)
                for z in range (0,x-1):
                    d = np.transpose(vo[:,z])@vo[:,z]
                    p = p + (np.transpose(v)@vo[:,z])*vo[:,z]/d

                v = v -p
                print("SIZE OF CORRECTION1:",np.linalg.norm(p))

        else:
            print("Error: beta is zero \a \a")
            sys.exit()


        ws = np.zeros(dim)
        ws=calculate_ws(v, N, Nu, Nd, tx, ty,U, Vx, Vy, one_dim, Nsu, Nsd, dim, statesu, statesd, statesu_dict, statesd_dict)
        a[x]=ws@v

        ws=ws-a[x]*v-b[x]*vl

        T_view[x,x]=a[x]
        T_view[x-1,x]=T_view[x,x-1]=b[x]
        wt,vt = get_eigen(T[:x+1,:x+1],vo[:,:x+1],False)
        idx = wt.argsort()[::1]
        wt=wt[idx]

        print("Step ",x,file=open(PATH+'log.txt','a'))
        print("Lowest eigenvalue:",wt[0],file=open(PATH+'log.txt','a'))

        print("Lowest eigenvalue:",wt[0])
        if x>2:
            print("2nd lowest eigenvalue:",wt[1])
            print("2nd lowest eigenvalue:",wt[1],file=open(PATH+'log.txt','a'))
        if x>3:
            print("3rd lowest eigenvalue:",wt[2])
            print("3rd lowest eigenvalue:",wt[2],file=open(PATH+'log.txt','a'))


        diff_0 = np.absolute(wt[0]-wto_0)
        diff_1 = np.absolute(wt[1]-wto_1)

        if x+1==dim:
            break
        if diff_0<=MAX_DELTA_E and diff_1<=MAX_DELTA_E:
            break




        wto_0 = wt[0]
        wto_1 = wt[1]


    vo[:,out_size]=v

    wt,vt = get_eigen(T[:out_size+1,:out_size+1],vo[:,:out_size+1])
    idx = wt.argsort()[::1]
    wt=wt[idx]
    print("Number of Lanczos Iterations ", out_size)
    print("Number of Lanczos Iterations ", out_size,file=open(PATH+'log.txt','a'))
    return T[:out_size+1,:out_size+1],vo[:,:out_size+1]

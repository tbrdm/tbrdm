# cython: language_level=3, boundscheck=False,initializedcheck=False, nonecheck=False
""" Hamiltonian.pyx
Author: Jonas Hachmeister
Last Change: 06.2020
About:
Methods which are used to calculate the elements of the hamiltonian. Used by Lanczos.pyx

 """
import sys
import time

import numpy as np
cimport numpy as np

import scipy.special
import scipy.sparse as sp
from sympy.utilities.iterables import multiset_permutations
from pandas import DataFrame
import cython

import TBRDM.bithacks as bit

bit.init()

def get_binary_states_bin(int N,int Nb):
    "Generates all permutations for the bitwise representation of a state and thus returns all possible states"
    print("Generated binary states")
    if Nb<N:
        start = (0,)
        for x in range(0,N-Nb-1):
            start=start+(0,)
        for x in range(0,Nb):
            start=start+(1,)
    else:
        start = (1,)
        for x in range(0,Nb-1):
            start=start+(1,)

    state_list=list(multiset_permutations(start))
    out=bit.list_to_bin(state_list)
    return out

cpdef tuple apply_non_diag_term_ring(int a, int i, int N, int Np):
    cdef int j,k,A
    cdef int bit1, bit2, b
    #iterate over all parts of the non-diag terms of the Hamiltoniam
    #-> the hopping terms

    bit1=bit.get_i_bit(i,a,N)
    bit2=bit.get_i_bit((i+1)%N,a,N)


    A=0
    b=0
    #hop to the left
    if bit1==0 and bit2==1:
        if i==N-1 and Np%2==0:
            A=-1
        else:
            A=1
        #get new state
        b = bit.flip_i_bit(i,a,N)
        b = bit.flip_i_bit((i+1)%N,b,N)
    #hop to the right
    elif bit1==1 and bit2==0:
        if i==N-1 and Np%2==0:
            A=-1
        else:
            A=1
        #get new state
        b = bit.flip_i_bit(i,a,N)
        b = bit.flip_i_bit((i+1)%N,b,N)
    return A,b

cpdef tuple apply_non_diag_term_ladder_vert(int a, int i, int N, int Np):
    "Returns the new State and the factor A which can be zero if no transition is possible"
    cdef int j,k,A
    cdef int bit1, bit2, b

    if i%2!=0:
        return 0,0

    bit1=bit.get_i_bit(i,a,N)
    bit2=bit.get_i_bit((i+1)%N,a,N)
    A=0
    b=0
    if (bit1==0 and bit2==1) or (bit1==1 and bit2==0):
        A=1
        b = bit.flip_i_bit(i,a,N)
        b = bit.flip_i_bit((i+1)%N,b,N)
    return A,b


cpdef tuple apply_non_diag_term_ladder_horiz(int a, int i, int N, int Np):
    "Returns the new State and the factor A which can be zero if no transition is possible"
    cdef int j,k,A
    cdef int bit1, bit2,bit3, b


    bit1=bit.get_i_bit(i,a,N)
    bit2=bit.get_i_bit((i+2)%N,a,N)
    bit3=bit.get_i_bit((i+1)%N,a,N)

    A=0
    b=0

    #hop to the left
    if (bit1==0 and bit2==1) or (bit1==1 and bit2==0):
        if i==N-2:
            if Np%2==0:
                if bit3==1:
                    A=1
                else:
                    A=-1
            else:
                if bit3==1:
                    A=-1
                else:
                    A=1
        elif i==N-1:
            if Np%2==0:
                if bit3==1:
                    A=1
                else:
                    A=-1
            else:
                if bit3==1:
                    A=-1
                else:
                    A=1
        else:
            if bit3==1:
                A=-1
            else:
                A=1
        #get new state
        b = bit.flip_i_bit(i,a,N)
        b = bit.flip_i_bit((i+2)%N,b,N)
    return A,b

cpdef float get_diag_term_ladder(int a,int b, int N,float U,float Vx,float Vy):
    "get the value of the on site interaction term for Ny=2"
    cdef int u = 0
    cdef int v = 0
    cdef int w = 0
    cdef int a_rot, b_rot, a_e, b_e, a_u, b_u
    #calculate number of electrons on same lattice site (u)
    u = bit.countSetBits(a&b)

    #calculate neighbor interaction
    #calculate horizontal neighbor interaction
    a_rot = bit.rotate_left(2,a,N)
    b_rot = bit.rotate_left(2,b,N)

    v = bit.countSetBits(a&a_rot)
    v += bit.countSetBits(b&b_rot)
    v += bit.countSetBits(a&b_rot)
    v += bit.countSetBits(b&a_rot)

    #calculate vertical neighbor interaction

    a_e = bit.get_even_bits(a)
    b_e= bit.get_even_bits(b)
    a_u = bit.get_uneven_bits(a)
    a_u = bit.rotate_left(1,a_u,N)
    b_u = bit.get_uneven_bits(b)
    b_u = bit.rotate_left(1,b_u,N)

    w = bit.countSetBits(a_e&a_u)
    w += bit.countSetBits(a_e&b_u)
    w += bit.countSetBits(b_e&b_u)
    w += bit.countSetBits(b_e&a_u)
    return U*u+Vx*v+Vy*w

cpdef float get_diag_term_ring(int a,int b,int N,float U,float Vx,float Vy):
    "get the value of the on site interaction term for Ny=1"
    cdef int u = 0
    cdef int v = 0
    cdef int w = 0
    cdef int a_rot, b_rot
    #calculate number of electrons on same lattice site (u)
    u = bit.countSetBits(a&b)
    #calculate number of neighbors
    a_rot=bit.rotate_left(1,a,N) #create copy of states which are rotated to the left
    b_rot=bit.rotate_left(1,b,N) #for easy comparision with &

    v = bit.countSetBits(a&a_rot)
    v += bit.countSetBits(b&b_rot)
    v += bit.countSetBits(a&b_rot)
    v += bit.countSetBits(b&a_rot)
    return U*u+Vx*v



cpdef int get_index(int x,int y,int N,int Ns):
    "get the index for the two-body hamiltonian"
    return x*scipy.special.binom(N,Ns)+y


cpdef tuple get_xy(int i,int Nsd):
    "Get x y for a z"
    cdef int x,y
    y = i%(Nsd)
    x =int((i-y)/(Nsd))
    return x,y



def one_body_Hamiltonian(N, N_IO, t_h, t_h2):
    "Returns the one-body hamiltonian as a sparse Matrix in the dok-Format"
    lattice = sp.dok_matrix((N,N),dtype=np.float32)

    for x in range(0,N-2):
        lattice[x,x+2] = -t_h2
        lattice[x+2,x] = -t_h2

    for x in range(0, N-1, 2):
        lattice[x+1,x]=lattice[x,x+1]=-t_h


    lattice[0,N-2]=lattice[N-2,0]=-t_h2
    lattice[1,N-1]=lattice[N-1,1]=-t_h2

    return lattice
